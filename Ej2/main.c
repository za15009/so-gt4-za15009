/*
* Ej2
* ALUMNO: Juan Carlos Zepeda Abrego
*/

#include <stdio.h>
#include <stdlib.h>

void imprimir(int *a) {
	*a = *a +1;
	printf("%d\n", *a);
}

int main(){
	int i, a = 0;
	for(i = 0; i<5; i++){
	imprimir(&a);
	}
return 0;
}
