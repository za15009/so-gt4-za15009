/*
* Ej10
* Juan Carlos Zepeda Abrego
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){

	int cantidad;
	
	//Estructura empleado
	typedef struct{
	char nombre[40];
	int dui;
	float sueldo;
	}empleado;

	empleado *array[1000];
	empleado *aux;

	printf("Cuántos registros serán?\n");
	scanf("%d",&cantidad);;

	//Ingresando datos
	for(int i=0; i<cantidad; i++){
		printf("Cual es el nombre del Empleado %d ?\n", i+1);
		 
		array[i]=(empleado *) malloc(cantidad*sizeof(empleado));
		
		scanf("%40s", array[i]->nombre);
		
		printf("Cual es el número de DUI del empleado %d ?\n", i+1);
		
		scanf("%d", &array[i]->dui);
		
		printf("Cual es el sueldo del empleado %d ?\n", i+1);
		
		scanf("%f", &array[i]->sueldo);
	}	
	
	//Ordenando datos
	system("clear");
	printf("Ordenando por Nombre\n");
	for(int i=0; i<cantidad; i++){
		for(int j=0; j<cantidad-1; j++){
			if(strcmp(array[j]->nombre, array[j+1]->nombre)>0){
				aux=array[j];
				array[j]=array[j+1];
				array[j+1]=aux;
			}
		}
	}
		
	//Mostrando datos
	for(int i=0; i<cantidad; i++){
		printf("%s, %d, %.2f\n",array[i]->nombre, array[i]->dui, array[i]->sueldo);
}

	//sueldo
	float suma=0;
	printf("Ordenando por Sueldo\n");
	for(int i=0; i<cantidad; i++){
		for(int j=0; j<cantidad-1; j++){
			if(array[j]->sueldo < array[j+1]->sueldo){
				aux=array[j];
				array[j]=array[j+1];
				array[j+1]=aux;
			}		
		}	
		suma=(suma+array[i]->sueldo);
	}
		suma=suma/cantidad;
		
		 
	for(int i=0;i<cantidad;i++){
		printf("%s, %d, %.2f.2\n", array[i]->nombre, array[i]->dui, array[i]->sueldo);
	}
		printf("Promedio de Sueldos:%.2f\n", suma);



return 0;
}
