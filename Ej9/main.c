/*
* Ej9
* Juan Carlos Zepeda Abrego
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(){

	int cantidad;

	//ingreso de cantidad de palabras
	printf("Qué cantidad de palabras usará?\n");
	scanf("%d", &cantidad);

	//Ingreso de palabras
	char *palabras[cantidad];
	system("clear");
	printf("Introduzca las frases\n");
	for(int i=0; i<cantidad; i++){
		palabras[i]=(char *) malloc(cantidad*sizeof(char));
		scanf("%s", palabras[i]);
	}

	//Ordenado de palabras
	char *aux;
	printf("Palabras Ordenadas\n");
	for(int i=0; i<cantidad; i++){
		for(int j=0; j<cantidad-1; j++){
			if(strcmp(palabras[j], palabras[j+1])>0){
				aux=palabras[j];
				palabras[j]=palabras[j+1];
				palabras[j+1]=aux;
			}
		}
	}

	for(int i=0; i<cantidad; i++){
		printf("%s\n", palabras[i]);
	}

return 0;
}
