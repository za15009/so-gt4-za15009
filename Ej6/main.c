/*
* Ej6
* Juan Carlos Zepeda Abrego
*/

#include <stdlib.h>
#include <stdio.h>

//funcion para determinar el número máximo
void maximo(float numeros[], int canNumeros){
	float aux;
	for(int i=0; i < canNumeros-1; i++){
		for(int j=0; j < canNumeros-i-1; j++){
			if(numeros[j+1]>numeros[j]){
			aux = numeros[j+1];
			numeros[j+1] = numeros[j];
			numeros[j]= aux;
			}
		}
	}
	printf("El número máximo es: %f\t", numeros[0]);
}

//funcion para encontrar número mínimo
void minimo(float numeros[], int canNumeros){
	int aux;
	for(int i = 0; i < canNumeros-1; i++){
		for(int j=0; j < canNumeros-i-j-1; j++){
			if(numeros[j+1]<numeros[j]){
				aux = numeros[j+1];
				numeros[j+1] = numeros[j];
				numeros[j] = aux;
			}
		}
	}
	printf("El número mínimo es: %f\t", numeros[0]);
}

//funcion para encontrar la media media aritmética
void media(float numeros[], int canNumeros){
	float media, acumulador;
	for(int i=0; i < canNumeros; i++){
		acumulador = acumulador+numeros[i];
	}
	media = acumulador/canNumeros;
	printf("La Media Aritmética es: %f\t", media);
}

int main(){

	float *numeros;
	int canNumeros, opcion;

	//Ingreso de cantidad de numeros
	printf("Cuántos números va a ingresar?\n");
	scanf("%d", &canNumeros);

	//Asignación de memoria dinámica al puntero
	numeros = (float *) malloc(canNumeros*sizeof(float));

	//Ingreso de numeros al puntero
	for(int i=0; i < canNumeros; i++){
		system("clear");
		printf("Ingrese el número de la posición:\t %d\n", i);
		scanf("%f", &numeros[i]);
	}

	system("clear");
	printf("Qué desea hacer con los números ?\n");
	printf("1. Encontrar el número máximo\n");
	printf("2. Encontrar el número mínimo\n");
	printf("3. Encontrar la media aritmética\n");
	printf("Presione Cualquier otro número para salir\n");
	scanf("%d", &opcion);

	system("clear");

	switch(opcion){
		case 1:
		maximo(numeros, canNumeros);
		break;

		case 2:
		minimo(numeros, canNumeros);
		break;

		case 3:
		media(numeros, canNumeros);
		break;
	}

return 0;
}
