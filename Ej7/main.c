/*
* Ej7
* Juan Carlos Zepeda Abrego
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//Funcion para evaluación del polinomio
void evaluacionDePolinomio(double coeficientes[], int grado){
	double valorBase, puntoX, acumulador;
	printf("En cual punto X desea evaluar el polinomio ?\t:");
	scanf("%lf", &puntoX);

	for(int i=0; i < grado; i++ ){
		valorBase = coeficientes[i];
		acumulador=acumulador+((valorBase)*(pow(puntoX, i)));
	}

	printf("El resultado evaluado en X=%f es P(X): %f donde:\n", puntoX, acumulador);
	printf("P(X)= ");
	for(int i = 0; i < grado; i++){
		if(coeficientes[i]!=0){
			if(i==0){
				printf("%.2f", coeficientes[i]);
			}else{
				if(coeficientes[i]<0){
					printf("%.2fX^%d", coeficientes[i], i);
				}else{
					printf("+ %2.fX^%d", coeficientes[i], i);
				}
			}
		}
	}
printf("\n");
}

int main(){
	double *coeficientes, coeficiente;
	int	grado;

	printf("Ingrese el grado del polinomio a resolver\n");
	scanf("%d", &grado);

	coeficientes = (double *) malloc(grado*sizeof(double));

	//ingreso de coeficientes
	for(int i=0; i < grado; i++){
		system("clear");
		printf("Ingrese el coeficiente para x%d:\t", i);
		scanf("%lf", &coeficiente);
		coeficientes[i] = coeficiente;
	}


	evaluacionDePolinomio(coeficientes, grado);

return 0;
}
