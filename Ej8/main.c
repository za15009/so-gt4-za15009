/*
* EJ8
* Juan Carlos Zepeda Abrego
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

char *peditTexto();
void contarVocales(char *, int[]);
void imprimir(int []);

char *pedirTexto(){
	char texto[255];
	char *canTexto;
	printf("Introduzca una oración:\n");
	fgets(texto,255,stdin);
	canTexto=texto;

return canTexto;
}

void contarVocales(char *texto, int *vocales){
	//a
	vocales[0]=0;
	//e
	vocales[1]=0;
	//i
	vocales[2]=0;
	//o
	vocales[3]=0;
	//u
	vocales[4]=0;

	while(*texto != '\0'){
		if(*texto=='a' || *texto=='A') vocales[0]=vocales[0]+1;
		if(*texto=='e' || *texto=='E') vocales[1]=vocales[1]+1;
		if(*texto=='i' || *texto=='I') vocales[2]=vocales[2]+1;
		if(*texto=='o' || *texto=='O') vocales[3]=vocales[3]+1;
		if(*texto=='u' || *texto=='U') vocales[4]=vocales[4]+1;
		texto++;
	}
}

void imprimir(int *num){
	printf("Cantidad vocal a:\t%d\n", num[0]);
	printf("Cantidad vocal e:\t%d\n", num[1]);
	printf("Cantidad vocal i:\t%d\n", num[2]);
	printf("Cantidad vocal o:\t%d\n", num[3]);
	printf("Cantidad vocal u:\t%d\n", num[4]);
}

int main(){
	char *texto;
	int num[5];
	texto = pedirTexto();
	contarVocales(texto, num);
	imprimir(num);
}
