/*
* Ej5
* Juan Carlos Zepeda Abrego
*/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int main(){

	int  canNumeros,aux;
	int* pNumeros;
	time_t t;
	//Toma una valor random de acuerdo al
	//momento de tiempo que se ejecutó el programa
	srand(time(&t));

	//Cantidad de números asignado de forma random.
	canNumeros = rand()%101;

	//Asignación de memoria dinámica
	pNumeros = (int *) malloc(canNumeros*sizeof(int));

	 //Llenando puntero dinámico con num. random
	printf("Numeros desordenados\n");
	for(int i = 0; i < canNumeros; i++){
		pNumeros[i] = rand()%100;
	}
	//Mostrando datos desordenados
	for(int i = 0; i < canNumeros; i++){
		printf("%d:\t %d\n", i, pNumeros[i]);
	}

	//Aplicando burbuja para ordenar
	for(int i = 0; i < canNumeros-1; i++){
		for(int j = 0; j < canNumeros-i-1; j++){
			if(pNumeros[j+1]>pNumeros[j]){
				aux=pNumeros[j+1];
				pNumeros[j+1]=pNumeros[j];
				pNumeros[j]=aux;
			}
		}
	}
	//Imprimiendo numeros ordenados
	printf("\n");
	printf("Numeros Ordenados:");
	for(int x = 0; x < canNumeros; x++){
		printf("%d\t", pNumeros[x]);
	}
	printf("\n");


return 0;
}
